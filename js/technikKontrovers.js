var app = angular.module('technikKontrovers', ['ui.bootstrap-slider', 'ui.bootstrap', 'chart.js']);
//use strict
app.controller('technikKontroversCtrl', ['$scope', 'databaseService', '$uibModal', '$location', function ($scope, databaseService, $uibModal, $location) {
    console.log("TK-App running!");

    $scope.location = $location;
    $scope.$watch('location.search()', function () {
        $scope.results = $location.absUrl().includes("results");
        $scope.personalResults = $location.absUrl().includes("personalResults");
        $scope.admin = $location.absUrl().includes("admin");

        if ($scope.results == true)
            showResults();
        else if ($scope.personalResults == true)
            $scope.state = "resultquery";
        else if ($scope.admin == true)
            $scope.state = "admin";

    }, true);

    $scope.state = 'survey';

    $scope.choices = {
        name: "",
        choice01: 4,
        choice02: 4,
        choice03: 4,
        choice12: 4,
        choice13: 4,
        choice23: 4
    };

    $scope.resetChoices = function () {
        $scope.choices = {
            name: "",
            choice01: 4,
            choice02: 4,
            choice03: 4,
            choice12: 4,
            choice13: 4,
            choice23: 4
        };
    };

    $scope.resultQuery = {
        name: ""
    };

    $scope.rawResults = [];
    $scope.slider = {
        "min": 0,
        "max": 8,
        "step": 1,
        "value": 5,
        "ticks": [0, 1, 2, 3, 4, 5, 6, 7, 8],
        "ticksLabels": ['Viel wichtiger', '', 'Eher wichtig', '', 'Gleich', '', 'Eher wichtig', '', 'Viel wichtiger']
    };

    //Soziale Gerechtigkeit, Umweltauswirkungen, Preis, Geschmack
    $scope.products = [
        {name: "Premium Goldauslese", properties: [0.74, 18.7, 18.99, 10]},
        {name: "Bio Wohlfühlkaffee", properties: [0.67, 17.8, 16.38, 5]},
        {name: "AntiKap Fair", properties: [0.9, 18.3, 13.58, 3]},
        {name: "Smart! Discount", properties: [0.62, 18.6, 11.58, 5]}
    ];

    $scope.submitAnswer = function (choices) {
        if (hasNull(choices)) {
            window.alert("Fehler bei der Eingabe des Namens!");
            return;
        }
        databaseService.getAllNames().then(function (res) {
            var names = res.data;
            var nameExists = false;
            names.forEach(function (obj) {
                console.log("name", obj.name, "choice.name", choices["name"], obj.name === "");
                if (obj.name === choices["name"]) {
                    nameExists = true;
                }
            });
            if (nameExists) {
                $scope.info = "Name leider schon vergeben oder ungültig. Bitte wählen Sie einen anderen Namen!";
                var modalInstance3 = $uibModal.open({
                    templateUrl: 'html/negativeModal.html',
                    size: 'md',
                    scope: $scope
                });
            }
            else if (!nameExists) {
                databaseService.saveAnswers(choices);
                $scope.resultQuery = {name: choices.name};
                $scope.personalResult($scope.resultQuery);
            }
        })
    };

    $scope.emptyDB = function () {
        databaseService.emptyDB();
        showResults();
    };

    $scope.personalResult = function (resultQuery) {
        if (hasNull(resultQuery)) {
            window.alert("Fehler bei der Eingabe des Namens!");
            return;
        }

        databaseService.getAllAnswers().then(function (res) {
            $scope.rawResults = res.data;
            $scope.count = res.data.length;

            var choice = $scope.rawResults.filter(function (as) {
                return as["name"] === resultQuery.name;
            })[0];

            if (!choice) {
                $scope.info = "Name leider nicht gefunden!";
                var modalInstance = $uibModal.open({
                    templateUrl: 'html/negativeModal.html',
                    size: 'md',
                    scope: $scope
                });
                $scope.state = "resultquery";
            } else {
                var ahpResults = calculateAHP(choice);
                visualizeAHP(ahpResults.geometricMeans)
                cis(ahpResults.geometricMeans);
                $scope.state = "personalresult"
            }
        });
    };

    $scope.showResults = function() {
      $scope.state = "results";
      showResults();
    };

    function showResults() {
        console.log("show results");
        databaseService.getAllAnswers().then(function (res) {
            $scope.rawResults = res.data;
            $scope.count = res.data.length;
            console.log("RawData", res.data);

            var geometricMeanAverages = [0, 0, 0, 0];
            $scope.rawResults.forEach(function (choice) {
                var ahpResults = calculateAHP(choice);
                geometricMeanAverages = geometricMeanAverages.map(function (v, i) {
                    return v + ahpResults.geometricMeans[i];
                })
            });
            geometricMeanAverages = geometricMeanAverages.map(function (v, i) {
                return v / res.data.length;
            });

            visualizeAHP(geometricMeanAverages);
            cis(geometricMeanAverages);
            $scope.state = "results";
        });
    }

    function hasNull(target) {
        console.log(target["name"], target["name"] && target["name"] === "");
        if (target["name"] === "") {
            return true;
        }
        for (var member in target) {
            if (target[member] === null)
                return true;
        }
        return false;
    }

    $scope.sumResults = function (userName) {
        var answers = $scope.rawResults.filter(function (as) {
            return as["name"] === userName;
        })[0]; //only first --> unique Names
        var sum = 0;
        if (!answers) {
            $scope.info = "Name leider nicht gefunden!";
            var modalInstance = $uibModal.open({
                templateUrl: 'html/negativeModal.html',
                size: 'md',
                scope: $scope
            });
        } else {
            Object.keys(answers).forEach(function (identifier) {
                if (identifier.indexOf("answer") !== -1) {
                    sum = sum + answers[identifier];
                }
            });
            $scope.score = 0;
            var modalInstance2 = $uibModal.open({
                templateUrl: 'html/scoreModal.html',
                size: 'md',
                scope: $scope
            });
        }
    };

    //----------------- AHP Calculations -----------------

    function calculateAHP(choices) {
        //Pairwise AHP Array
        var proportions = [
            getProportion(choices.choice01),
            getProportion(choices.choice02),
            getProportion(choices.choice03),
            getProportion(choices.choice12),
            getProportion(choices.choice13),
            getProportion(choices.choice23)
        ];

        //AHP Matrix
        var matrix = [
            [1, proportions[0], proportions[1], proportions[2]],
            [(1 / proportions[0]), 1, proportions[3], proportions[5]],
            [(1 / proportions[1]), (1 / proportions[3]), 1, proportions[4]],
            [(1 / proportions[2]), (1 / proportions[5]), (1 / proportions[4]), 1]
        ];
        console.log("matrix", matrix);

        //Normalize matrix by column sum
        normalizedMatrix = normalizeByColumnSum(matrix);
        console.log("normalizedMatrix", normalizedMatrix);

        //Average and geometric mean
        averages = normalizedMatrix.map(average);
        geometricMeans = normalizedMatrix.map(geometricMean);
        console.log("geometricMeans", geometricMeans);

        //corresponding eigenvektor
        var eigenvektor = math.multiply(matrix, averages);

        //consistency proportion
        var dividedEigenvektorAvg = average([0, 1, 2, 3].map(function (i) {
            return eigenvektor[i] / averages[i]
        }));
        var consistencyProportion = (dividedEigenvektorAvg - 4) / 27; //TODO: check?
        var roundedConsistency = Math.round(consistencyProportion * 10) / 10;

        return {
            averages: averages,
            geometricMeans: geometricMeans,
            consistency: roundedConsistency
        };
    }

    /**
     choice: zero-based
     **/
    function getProportion(choice) {
        if (choice >= 0 && choice <= 4)
            return -2 * choice + 9;
        else if (choice >= 4 && choice <= 8)
            return 1 / (2 * (choice - 3) - 1);
        else
            return 0;
    }

    /**
     matrix: a two-dimensional array
     column: zero-based column index
     */
    function columnQuadSum(matrix, column) {
        var avg = average(matrix.map(function (row) {
            return row[column]
        }));

        var quadSum = 0.0;
        for (var row = 0; row < matrix.length; row++)
            quadSum = quadSum + Math.pow(matrix[row][column], 2);

        return quadSum;
    }

    /**
     matrix: a two-dimensional array
     column: zero-based column index
     */
    function columnSum(matrix, column) {
        var sum = 0.0;
        for (var row = 0; row < matrix.length; row++)
            sum = sum + matrix[row][column];

        return sum;
    }

    /* Ersetzt
     function getColumn(matrix, column) {
     return matrix.map((row) => row[column]);
     }
     */
    function getColumn(matrix, column) {
        return matrix.map(function (row) {
            return row[column];
        });
    }

    function average(array) {
        var sum = array.reduce(function (a, b) {
            return a + b;
        });
        return sum / array.length;
    }

    function geometricMean(array) {
        var product = array.reduce(function (a, b) {
            return a * b
        }, 1.0);
        return Math.pow(product, 1 / array.length);
    }

    function normalizeByColumnSum(matrix) {
        columnSums = [
            columnSum(matrix, 0),
            columnSum(matrix, 1),
            columnSum(matrix, 2),
            columnSum(matrix, 3)
        ];
        return normalizePerColumn(matrix, columnSums);
    }

    function normalizePerColumn(matrix, factors) {
        var result = [[-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1]];
        for (var row = 0; row < matrix.length; row++) {
            for (var col = 0; col < matrix[row].length; col++) {
                result[row][col] = matrix[row][col] / factors[col];
            }
        }

        return result;
    }

    function euclideanDistance(x, y) {
        var sum = 0;
        for (var i = 0; i < x.length; i++)
            sum += Math.pow(x[i] - y[i], 2);

        return Math.sqrt(sum);
    }

    function visualizeAHP(data) {
        $scope.labels = ['Soziale Gerechtigkeit', 'Umweltauswirkungen', 'Preis', 'Geschmack'];
        $scope.series = ['Geometrisches Mittel'];
        $scope.data = [data];
    }

    function visualizeCIS(data) {
        //$scope.labels_product = $scope.products.map((product) => product.name); ERSETZT
        $scope.labels_product = $scope.products.map(function (product) {
            return product.name;
        });
        $scope.series_product = ['Übereinstimmung'];
        $scope.data_product = [data];
    }

    function cis(geometricMeans) {
        //Produktmatrix
        var productMatrix = $scope.products.map(function (product) {
            return product.properties
        });

        //Normierungswerte bestimmen
        //var normFactors = [0,1,2,3].map((col) => Math.sqrt(columnQuadSum(productMatrix, col))); ERSETZT
        var normFactors = [0, 1, 2, 3].map(function (col) {
            return Math.sqrt(columnQuadSum(productMatrix, col));
        });

        //Produkte normieren
        var normalizedProductMatrix = normalizePerColumn(productMatrix, normFactors);

        //Durch AHP Ergebnis (geometrisches Mittel) teilen
        var weightedProductMatrix = normalizePerColumn(normalizedProductMatrix,
          //geometricMeans.map((x) = > (1 / x))
          geometricMeans.map(function (x) {
              return (1 / x);
          })
        );

        //Best/Worst bestimmen
        var best = [
            Math.max.apply(null, getColumn(weightedProductMatrix, 0)),
            Math.min.apply(null, getColumn(weightedProductMatrix, 1)),
            Math.min.apply(null, getColumn(weightedProductMatrix, 2)),
            Math.max.apply(null, getColumn(weightedProductMatrix, 3))
        ];

        var worst = [
            Math.min.apply(null, getColumn(weightedProductMatrix, 0)),
            Math.max.apply(null, getColumn(weightedProductMatrix, 1)),
            Math.max.apply(null, getColumn(weightedProductMatrix, 2)),
            Math.min.apply(null, getColumn(weightedProductMatrix, 3))
        ];

        //CIS-Vektor berechnen
        /* ERSETZT
         var dplus = range($scope.products.length).map((productIndex) = >
         euclideanDistance(weightedProductMatrix[productIndex], best)
         );
         var dminus = range($scope.products.length).map((productIndex) = >
         euclideanDistance(weightedProductMatrix[productIndex], worst)
         );
         var cis = range($scope.products.length).map((productIndex) = >
         dminus[productIndex] / (dplus[productIndex] + dminus[productIndex])
         );
         */

        var dplus = range($scope.products.length).map(function (productIndex) {
            return euclideanDistance(weightedProductMatrix[productIndex], best);
        });
        var dminus = range($scope.products.length).map(function (productIndex) {
            return euclideanDistance(weightedProductMatrix[productIndex], worst);
        });
        var cis = range($scope.products.length).map(function (productIndex) {
            return dminus[productIndex] / (dplus[productIndex] + dminus[productIndex]);
        });
        visualizeCIS(cis);
    }


    function range(upperBound) {
        return Array.apply(null, Array(upperBound)).map(function (_, i) {
            return i;
        });
    }

    function calcDPlus() {
        return Math.sqrt();
    }
}])

  .service('databaseService', function ($http, $q) {
      this.emptyDB = function () {
          console.log("dbservice empty db");
          var deferred = $q.defer();
          $http({
              method: 'GET',
              url: '/emptyDB'
          }).success(function (data) {
              deferred.resolve({'data': data});
          }).error(function () {
              window.alert("EmptyDB GET failure!");
          });
          return deferred.promise;
      };
      this.saveAnswers = function (answers) {
          var deferred = $q.defer();
          $http({
              url: '/saveAnswers',
              method: "POST",
              data: answers
          })
            .then(function (response) {
                  deferred.resolve(response);
              },
              function (response) { // optional
                  //window.alert("Answer save failure! " + response);
                  console.log("Answer save failure! ", response);
              });
          return deferred.promise;
      };
      this.getAllAnswers = function () {
          var deferred = $q.defer();
          $http({
              method: 'GET',
              url: '/getAllAnswers'
          }).success(function (data) {
              console.log("GET Answers", data);
              deferred.resolve({'data': data});
          }).error(function () {
              window.alert("GetAnswers GET failure!");
          });
          return deferred.promise;
      };
      this.getAllNames = function () {
          var deferred = $q.defer();
          $http({
              method: 'GET',
              url: '/getAllNames'
          }).success(function (data) {
              console.log("GET Names", data);
              deferred.resolve({'data': data});
          }).error(function () {
              window.alert("GetNames GET failure!");
          });
          return deferred.promise;
      };
  });
